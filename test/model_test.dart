import 'package:BabelTowerWords/model/model.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group("testing the model Word", () {
    test('serialization and equal works', () {
      Word a = new Word();
      a.put("en", "pen");
      a.put("it", "penna");

      Word b = Word.fromJson(a.toJson());
      Word c = Word.fromJson(a.toJson());
      expect(c == b, true);
      expect(c == a, true);
      expect(b == a, true);
    });
    test('Set', () {
      Word a = new Word();
      a.put("en", "pen");
      a.put("it", "penna");

      Word b = Word.fromJson(a.toJson());
      Word c = Word.fromJson(a.toJson());
      Set<Word> set = new Set<Word>();
      set.add(a);
      set.add(b);
      set.add(c);

      expect(set.length, 1);
    });
  });
}
