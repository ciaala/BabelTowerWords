import 'dart:convert';

import 'package:http/http.dart' as http;

import '../model/model.dart';

class WebService {
  static final String url = 'https://ciaala.internet-box.ch/sessions?sessionId=';
  // static final String url = 'http://192.168.1.7/sessions?sessionId=';

  static Future<List<Word>> loadWords() async {
    final String sessionId = 'words';
    final response = await http.get(url + sessionId);
    print('Response Status: ${response.statusCode}');
    print('Response Body: ${response.body}');

    final List<dynamic> json = jsonDecode(response.body);
    final Set<Word> result = <Word>{};
    if (json != null) {
      json.forEach((element) {
        print('element: $element');

        final word = Word.fromJson(element);
        if (word.isValid()) {
          result.add(word);
        }
        print('word: $word');
      });
    }
    return result.toList(growable: false);
  }

  static Future<List<String>> loadLanguages() async {
    final String sessionId = 'languages';
    final response = await http.get(url + sessionId);
    print('Response Status: ${response.statusCode}');
    print('Response Body: ${response.body}');
    final List<dynamic> json = jsonDecode(response.body);
    final Set<String> result = <String>{};
    if (json != null) {
      result.addAll(json.where((element) => element is String).map((e) => e as String));
    }
    return result.toList(growable: false);
  }

  static postWord(Word word) async {
    final String sessionId = 'words';

    final response = await http.get(url + sessionId);
    print('Response Status: ${response.statusCode}');
    print('Response Body: ${response.body}');

    final List<dynamic> words = jsonDecode(response.body);
    words.add(word.toJson());
    print("words " + words.toString());
    final update = await http.post(
      url + sessionId,
      headers: {"Content-Type": "application/json; charset=UTF-8"},
      body: jsonEncode(words),
    );
    print('Response Status: ${update.statusCode}');
    print('Response Body: ${update.body}');
  }
}
