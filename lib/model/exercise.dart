import 'package:tuple/tuple.dart';

import 'model.dart';

/// The result of running an exercise
/// a User (uuid) runs at date and time (dateTime) an exercise (uuid)
/// and for each words we collect each word
///

class ExerciseRun {
  final DateTime dateTime = DateTime.now();
  final String userUuid;
  final String motherLanguage;
  final List<String> exercisesLanguages;
  final Exercise exercise;
  final ExerciseResult exerciseResult = new ExerciseResult();

  ExerciseRun(this.userUuid, this.motherLanguage, this.exercisesLanguages, this.exercise);
}

class Exercise {
  final String exerciseUuid;
  final List<Word> words;

  Exercise(this.exerciseUuid, this.words);
}

class WordResult {
  final String wordUuid;
  final Map<String, Tuple2<bool, String>> languageResultMap;

  WordResult(this.wordUuid, this.languageResultMap);
}

class ExerciseResult {
  final List<WordResult> wordResults = <WordResult>[];

  void addWordResult(final String wordUuid, final Map<String, Tuple2<bool, String>> wordResults) {
    final WordResult wordResult = new WordResult(wordUuid, wordResults);
    this.wordResults.add(wordResult);
  }
}
