import 'dart:convert';

import 'package:basic_utils/basic_utils.dart';
import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';

class Word {
  final Map<String, String> _languageTranslationMap = <String, String>{};

  static Word fromJson(Map<String, dynamic> data) {
    // Map<String, dynamic> data = jsonDecode(payload);
    final Word w = new Word();
    data.entries.where((element) {
      final type = element.value.toString().runtimeType;
      print('type: ' + type.toString());
      return (element.value is String);
    }).forEach((element) {
      w._languageTranslationMap.putIfAbsent(element.key, () => element.value as String);
    });
    print('w: ' + w._languageTranslationMap.toString());
    return w;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = new Map<String, dynamic>();
    map.addAll(this._languageTranslationMap);
    return map;
  }

  @override
  String toString() {
    StringBuffer result = new StringBuffer();
    final List<String> sortedKeys = _languageTranslationMap.keys.toList();
    result.write("Word[");
    sortedKeys.sort((a, b) => a.compareTo(b));
    sortedKeys.forEach((element) {
      result.write(element);
      result.write(' = "');
      result.write(_languageTranslationMap[element]);
      result.write('", ');
    });
    result.write(']');
    return result.toString();
  }

  String get(String language) {
    return _languageTranslationMap[language];
  }

  void put(String language, String definition) {
    _languageTranslationMap.putIfAbsent(language, () => definition);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    final result = other is Word &&
        runtimeType == other.runtimeType &&
        mapEquals(_languageTranslationMap, other._languageTranslationMap);
    return result;
  }

  @override
  int get hashCode {
    return MapEquality().hash(_languageTranslationMap);
  }

  bool isValid() {
    if (_languageTranslationMap.isEmpty) {
      return false;
    }
    return _languageTranslationMap.entries
        .every((element) => StringUtils.isNotNullOrEmpty(element.key) && StringUtils.isNotNullOrEmpty(element.value));
  }

  static Word fromJsonString(String jsonString) {
    return fromJson(jsonDecode(jsonString));
  }
}
