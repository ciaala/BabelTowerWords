import 'package:BabelTowerWords/model/exercise.dart';
import 'package:BabelTowerWords/model/model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tuple/tuple.dart';

import 'static.dart';

class ExerciseResultsPage extends StatefulWidget {
  final ExerciseRun _exerciseRun;

  ExerciseResultsPage(this._exerciseRun);

  @override
  State<ExerciseResultsPage> createState() => _ExerciseResultsState(this._exerciseRun);
}

class _ExerciseResultsState extends State<ExerciseResultsPage> {
  final ExerciseRun _exerciseRun;

  _ExerciseResultsState(this._exerciseRun);

  Widget _buildRow(
      final String wordInMotherLanguage, final Word currentWord, final Map<String, Tuple2<bool, String>> results) {
    assert(results != null);
    print("_buildRow " + {wordInMotherLanguage, results}.toString());
    //List<Icon> languages = List<Icon>[];
    final List<Widget> icons = results.entries.map((element) {
      if (element.value.item1) {
        //return Icon(Icons.flag_outlined, color: Colors.green);
        return makeFlag(element.key);
      } else {
        return Padding(
          padding: EdgeInsets.fromLTRB(0, 0, 16, 0),
          child: IconButton(
            icon: Icon(Icons.flag_sharp, color: Colors.red),
            tooltip: currentWord.get(element.key) + " != " + element.value.item2,
            onPressed: null,
          ),
        );
      }
    }).toList();

    print("icons:" + icons.length.toString() + " " + icons.toString());
    final List<Widget> row = <Widget>[];
    bool failed = results.values.any((element) => !element.item1);
    final Icon icon = failed
        ? Icon(Icons.indeterminate_check_box_outlined, color: Colors.red, size: 32)
        : Icon(Icons.check_circle_outline, color: Colors.green, size: 32);
    row.add(Padding(
      padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
      child: icon,
    ));
    row.add(Expanded(child: Text(wordInMotherLanguage)));
    row.addAll(icons);
    return Row(
      children: row,
    );
  }

  @override
  Widget build(BuildContext context) {
    print("ExerciseResultsPage.build");
    final ExerciseResult exerciseResult = _exerciseRun.exerciseResult;
    final List<WordResult> wordResults = exerciseResult.wordResults;

    final List<Word> exerciseWords = _exerciseRun.exercise.words;
    print("ExerciseResultsPage.build.builder");

    return ListView.builder(
        padding: EdgeInsets.all(16.0),
        itemCount: _exerciseRun.exercise.words.length + 1,
        itemBuilder: /*1*/ (context, index) {
          if (index == 0) {
            return Row(
              children: [
                Padding(padding: EdgeInsets.fromLTRB(64, 0, 0, 0), child: makeFlag(_exerciseRun.motherLanguage)),
                Expanded(child: Text('')),
                makeFlag('it'),
              ],
            );
          }
          index--;
          final Word currentWord = exerciseWords[index];
          // final String word = currentWord.get(_exerciseRun.motherLanguage);
          final WordResult wordResult = wordResults[index];
          //  final Map<String,> languageResultMap =

          return _buildRow(currentWord.get(_exerciseRun.motherLanguage), currentWord, wordResult.languageResultMap);
        });
  }
}
