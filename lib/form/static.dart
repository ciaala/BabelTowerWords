import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

final Map<String, String> _languageToFlag = <String, String>{'en': 'gb', 'it': 'it'};

Widget makeFlag(final String language) {
  final String contextPath = '/assets/packages/country_icons/';
  final String type = 'svg';
  final String path = 'icons/flags/' + type + '/' + _languageToFlag[language] + '.' + type;

  final bool containsKey = _languageToFlag.containsKey(language);
  print("flag: " + language + "  " + containsKey.toString());
  if (containsKey) {
    //return makeSvgString();
    //return makeSvgAsset(contextPath, path);
    //return makeImageAsset(contextPath, path);
    return makeImageNetwork(contextPath, path);
  } else {
    return Icon(Icons.emoji_flags);
  }
}

Widget makeSvgAsset(final String contextPath, final String path) {
  return SvgPicture.asset(
    path,
    package: 'country_icons',
    placeholderBuilder: (context) => CircularProgressIndicator(
      strokeWidth: 1.0,
    ),
    height: 64,
    width: 64,
  );
}

Widget makeImageAsset(
  final String contextPath,
  final String path,
) {
  return Image.asset(
    path,
    package: "country_icons",
    width: 48,
    height: 48,
  );
}

Widget makeImageNetwork(
  final String contextPath,
  final String path,
) {
  return Image.network(
    contextPath + path,
    width: 48,
    height: 48,
  );
}

Widget makeSvgString() {
  return SvgPicture.string('''<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3 2">
<rect width="1" height="2" fill="#009246"/>
<rect width="1" height="2" x="1" fill="#fff"/>
<rect width="1" height="2" x="2" fill="#ce2b37"/>
</svg>''');
}

Widget makeSvgNetwork(
  final String contextPath,
  final String path,
) {
//  final String dogUrl = 'https://upload.wikimedia.org/wikipedia/commons/7/73/Alarm_Clock_Vector.svg';
//'http://www.svgrepo.com/show/2046/dog.svg';

  return SvgPicture.network(
    contextPath + path,
    width: 128,
    height: 128,
    placeholderBuilder: (context) => CircularProgressIndicator(
      strokeWidth: 1.0,
    ),
  );
}
