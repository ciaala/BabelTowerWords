import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:tuple/tuple.dart';

import './ExerciseResultsPage.dart';
import '../model/exercise.dart';
import '../model/model.dart';
import 'static.dart';

class ExerciseStepPage extends StatefulWidget {
  final ExerciseRun _exerciseRun;

  ExerciseStepPage(this._exerciseRun);

  @override
  State<ExerciseStepPage> createState() => _ExerciseStepState(this._exerciseRun);
}

class _ExerciseStepState extends State<ExerciseStepPage> {
  final ExerciseRun _exerciseRun;
  final Map<String, TextFormField> _formFields = <String, TextFormField>{};

  int _index = 0;

  _ExerciseStepState(this._exerciseRun);

  void _clear() {
    _formFields.forEach((key, field) {
      field.controller.clear();
    });
  }

  void _validateWord() {
    print("_validateWord");
    final Word word = _exerciseRun.exercise.words[_index];
    print("word " + word.toString());
    print("Form entries: " + _formFields.entries.length.toString());

    final Map<String, Tuple2<bool, String>> results = {};

    final bool result = _formFields.entries.every((entry) {
      final String actual = entry.value.controller.value.text;
      final String language = entry.key;
      print("entry: " + {entry.key, entry.value}.toString());
      final String expected = word.get(language);
      print("expected: " + expected);

      final bool sameWord = StringUtils.equalsIgnoreCase(expected, actual);

      results[language] = new Tuple2(sameWord, actual);
      print("result:" + language + ": " + results[language].toString());
      if (!sameWord) {
        print("wrong " + language);
      } else {
        print("correct " + language);
      }
      print({expected, actual, sameWord});
      return sameWord;
    });
    print("result: " + result.toString());
    _exerciseRun.exerciseResult.addWordResult(word.get("en"), results);
    print(_exerciseRun.exerciseResult);

    setState(() {
      _index = _index + 1;
      print("Index " + _index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget form;
    List<Word> words = _exerciseRun.exercise.words;

    if (_index < words.length) {
      form = Center(
        child: Column(
          children: _buildExerciseStep(),
        ),
      );
    } else {
      print("BuildingExerciseResult");
      form = Center(
        child: new ExerciseResultsPage(_exerciseRun),
      );
    }
    return form;
  }

  List<Widget> _buildExerciseStep() {
    final List<Widget> form = <Widget>[];

    print("ExercisePage.build");
    print("Index " + _index.toString());
    final Word word = _exerciseRun.exercise.words[_index];
    print("word " + _index.toString());

    final String wordInMotherLanguage = word.get(_exerciseRun.motherLanguage);
    final TextFormField motherLanguageField = TextFormField(
      key: Key(wordInMotherLanguage),
      decoration: InputDecoration(
        icon: makeFlag(_exerciseRun.motherLanguage),
        labelText: _exerciseRun.motherLanguage,
        contentPadding: EdgeInsets.all(16.0),
        enabled: false,
      ),
      initialValue: wordInMotherLanguage,
    );

    form.add(motherLanguageField);

    for (final String language in _exerciseRun.exercisesLanguages) {
      final TextFormField textFormField = TextFormField(
        key: Key(wordInMotherLanguage + language),
        controller: TextEditingController(),
        decoration: InputDecoration(
          icon: makeFlag(language),
          labelText: language,
          contentPadding: EdgeInsets.all(16.0),
        ),
      );
      _formFields[language] = textFormField;
      form.add(textFormField);
    }

    form.add(ButtonBar(
      buttonPadding: EdgeInsets.only(left: 24, right: 24),
      children: [
        FlatButton(
          child: Text('Clear'),
          onPressed: _clear,
        ),
        RaisedButton(
          child: Text('Validate'),
          onPressed: _validateWord,
        ),
      ],
    ));
    return form;
  }
}
