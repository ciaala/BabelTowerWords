import 'package:BabelTowerWords/service/sessionApi.dart';
import 'package:flutter/material.dart';

import '../model/model.dart';

class WordForm extends StatefulWidget {
  final List<String> _languages;

  WordForm(this._languages);

  @override
  State<WordForm> createState() => _WordFormState(this._languages);
}

class _WordFormState extends State<WordForm> {
  final subTitle = 'Insert new Word';
  final List<String> _languages;
  final Map<String, String> _definitions = <String, String>{};
  final List<TextFormField> _formFields = <TextFormField>[];

  _WordFormState(this._languages);

  void _clear() {
    _formFields.forEach((field) {
      field.controller.clear();
    });
  }

  void _insertWord() {
    print('insertWord');
    final Word word = new Word();
    _definitions.forEach((key, value) {
      word.put(key, value);
    });
    WebService.postWord(word);
  }

  void _updateLanguageDefinition(final String language, final String definition) {
    _definitions[language] = definition;
  }

  Widget build(BuildContext context) {
    final List<Widget> form = <Widget>[];
    for (final String language in _languages) {
      _formFields.add(TextFormField(
        controller: TextEditingController(),
        decoration: InputDecoration(
          labelText: language,
          contentPadding: EdgeInsets.all(16.0),
        ),
        onChanged: (value) => _updateLanguageDefinition(language, value),
      ));
    }
    form.addAll(_formFields);
    form.add(ButtonBar(
      children: [
        FlatButton(
          child: Text('Clear'),
          onPressed: _clear,
        ),
        RaisedButton(
          child: Text('Add Word'),
          onPressed: _insertWord,
        ),
      ],
    ));
    return Center(
      child: Column(
        children: form,
      ),
    );
  }
}
