// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:BabelTowerWords/form/ExerciseResultsPage.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:tuple/tuple.dart';

import 'form/ExerciseStepPage.dart';
import 'form/WordForm.dart';
import 'model/exercise.dart';
import 'model/model.dart';
import 'service/sessionApi.dart';

void main() => runApp(MyApp());

// #docregion MyApp
class MyApp extends StatelessWidget {
  final title = 'Babel Tower: Words';

  // #docregion build

  Widget _buildExerciseResultsPage() {
    List<Word> words = <Word>[
      Word.fromJsonString('{"en":"pen", "it":"penna"}'),
      Word.fromJsonString('{"en":"pencil", "it":"matita"}'),
    ];
    final Exercise exercise = new Exercise("exe-uuid", words);
    final ExerciseRun exerciseRun = ExerciseRun("user-uuid", "en", <String>["it"], exercise);
    final Map<String, Tuple2<bool, String>> result = new Map();
    result['it'] = new Tuple2(false, 'salame');

    exerciseRun.exerciseResult.wordResults.add(
      new WordResult('en-pen', result),
    );
    exerciseRun.exerciseResult.wordResults.add(
      new WordResult('en-pencil', {'it': new Tuple2(true, 'matita')}),
    );
    return Scaffold(
      appBar: AppBar(title: Text('Saved Suggestions')),
      body: ExerciseResultsPage(exerciseRun),
    );
  }

  @override
  Widget build(BuildContext context) {
    bool isBuildCustom = false;

    return MaterialApp(
      title: title,
      home: isBuildCustom ? _buildExerciseResultsPage() : RandomWords(title),
      debugShowCheckedModeBanner: false,
    );
  }
}
// #enddocregion build

class RandomWords extends StatefulWidget {
  final String _title;

  RandomWords(this._title);

  @override
  State<RandomWords> createState() => _RandomWordsState(this._title, "en");
}

// #enddocregion MyApp
// #docregion RWS-var
class _RandomWordsState extends State<RandomWords> {
  bool _initialized = false;
  final List<Word> _suggestions = <Word>[];
  final Set<Word> _saved = <Word>{};
  final TextStyle _biggerFont = TextStyle(fontSize: 18.0);
  final LocalStorage _localStorage = new LocalStorage('BabelWords');
  static const String DEFAULT_LANGUAGE = "en";
  static const String KEY_SAVED_WORDS = "favoriteWords";
  final String _title;
  final String _language;
  final Future<List<Word>> futureForWords = WebService.loadWords();

  _RandomWordsState(this._title, this._language);

  // #enddocregion RWS-var

  // #docregion _buildSuggestions
  Widget _buildSuggestions() {
    print("_buildSuggestions");
    return ListView.builder(
        itemCount: _suggestions.length * 2,
        padding: EdgeInsets.all(16.0),
        itemBuilder: /*1*/ (context, i) {
          if (i.isOdd) return Divider();
          /*2*/
          final index = i ~/ 2; /*3*/
          if (index >= _suggestions.length) {
            return Text('');
          } else {
            return _buildRow(_suggestions[index]);
          }
        });
  }

// #enddocregion _buildSuggestions

// #docregion _buildRow
  Widget _buildRow(Word word) {
    final alreadySaved = _saved.contains(word);
    return ListTile(
      title: Text(
        word.get(this._language),
        style: _biggerFont,
      ),
      leading: IconButton(
        icon: Icon(
          alreadySaved ? Icons.favorite : Icons.favorite_border,
          color: alreadySaved ? Colors.red : null,
        ),
        onPressed: () {
          setState(() {
            if (alreadySaved) {
              _saved.remove(word);
            } else {
              _saved.add(word);
            }
            final List<String> words = _saved.map((e) => e.get(DEFAULT_LANGUAGE)).toList();
            _localStorage.setItem(KEY_SAVED_WORDS, words);
          });
        },
      ),
      trailing: IconButton(
          icon: Icon(
            Icons.auto_delete_outlined,
            color: Colors.redAccent,
          ),
          onPressed: () {
            setState(() {});
          }),
      onTap: () {
        setState(() {
          if (alreadySaved) {
            _saved.remove(word);
          } else {
            _saved.add(word);
          }
        });
      },
    );
  }

// #enddocregion _buildRow
  void _showSuggestions() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          final tiles = _saved.map((Word word) {
            return ListTile(title: Text(word.get(_language), style: _biggerFont));
          });
          final divided = ListTile.divideTiles(
            context: context,
            tiles: tiles,
          ).toList();

          return Scaffold(
            appBar: AppBar(title: Text('Saved Suggestions')),
            body: ListView(children: divided),
          );
        },
      ),
    );
  }

  void _addNewWord() async {
    String received = await Navigator.of(context).push(
      MaterialPageRoute<String>(
        builder: (BuildContext context) {
          final List<String> _languages = ['en', 'it'];
          return Scaffold(
            appBar: AppBar(title: Text('add New Word')),
            body: WordForm(_languages),
          );
        },
      ),
    );
    // TODO: Trying to reload the collection of words
    print("calling set state");
    _suggestions.clear();
    setState(() {});
  }

  void _showExercises() {
    Navigator.of(context).push(
      MaterialPageRoute<String>(
        builder: (BuildContext context) {
          final List<String> _languages = ['it'];
          final List<Word> words = getExerciseWords();

          final Exercise exercise = Exercise("exerciseUuid", words);
          var exerciseRun = ExerciseRun("userUuid", "en", _languages, exercise);
          return Scaffold(
            appBar: AppBar(
              title: Text('Exercise'),
              actions: <Icon>[
                Icon(Icons.functions),
              ],
            ),
            body: ExerciseStepPage(exerciseRun),
          );
        },
      ),
    );
  }

// #docregion RWS-build
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Future.wait([futureForWords, _localStorage.ready]),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            if (_suggestions.isEmpty) {
              _suggestions.addAll(snapshot.data[0]);
            }
          } else if (snapshot.hasError) {
            return Text(
              'There was an error',
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }

          if (!_initialized) {
            var items = _localStorage.getItem(KEY_SAVED_WORDS);
            print("items " + items.toString());
            final List<String> savedWords = new List<String>();
            if (items != null) {
              savedWords.addAll((items as List).map((item) => item as String).toSet());
            }
            if (savedWords.isNotEmpty) {
              _saved.addAll(_suggestions.where((element) => savedWords.contains(element.get(DEFAULT_LANGUAGE))));
            }
            _initialized = true;
          }

          return Scaffold(
            appBar: AppBar(
              title: Text(_title + ' Home Page'),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.list, color: Colors.white, semanticLabel: "Words"),
                  onPressed: _showSuggestions,
                ),
              ],
              leading: IconButton(
                icon: Icon(Icons.play_arrow_outlined, color: Colors.white, semanticLabel: "play"),
                onPressed: _showExercises,
              ),
            ),
            body: _buildSuggestions(),
            floatingActionButton: FloatingActionButton(
              onPressed: _addNewWord,
              tooltip: 'Increment',
              child: Icon(Icons.add),
            ),
            persistentFooterButtons: <Widget>[
              IconButton(
                icon: Icon(Icons.play_arrow_outlined, color: Colors.blue, semanticLabel: "play"),
                onPressed: _showExercises,
              ),
              IconButton(
                icon: Icon(Icons.list, color: Colors.orange, semanticLabel: "Words"),
                onPressed: _showSuggestions,
              ),
            ],
          );
        });
  }

  List<Word> getExerciseWords() {
    final List<Word> words = List<Word>();
    if (_saved.isNotEmpty) {
      words.addAll(_saved);
    } else {
      final List<Word> list = _suggestions.toList();
      list.shuffle();
      words.addAll(list.sublist(0, 4));
    }
    return words;
  }
}
// #enddocregion RWS-build
// #docregion RWS-var

// #enddocregion RWS-var
